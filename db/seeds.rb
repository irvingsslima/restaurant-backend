Product.destroy_all
ProductCategory.destroy_all
Restaurant.destroy_all
Category.destroy_all

puts 'Criando Categorias'

path_image = 'public/images/categories/mexican.jpg'
c = Category.create(id: 1, title: 'mexicana')
c.image.attach(io: File.open(path_image), filename: 'mexican.jpg')

path_image = 'public/images/categories/italian.jpeg'
c = Category.create(id: 2, title: 'italiana')
c.image.attach(io: File.open(path_image), filename: 'italian.jpeg')

path_image = 'public/images/categories/japonese.jpeg'
c = Category.create(id: 3, title: 'japonesa')
c.image.attach(io: File.open(path_image), filename: 'japonese.jpeg')

path_image = 'public/images/categories/vegan.jpeg'
c = Category.create(id: 4, title: 'vegana')
c.image.attach(io: File.open(path_image), filename: 'vegan.jpeg')


path_image = 'public/images/categories/peruvian.jpg'
c = Category.create(id: 5, title: 'vegana')
c.image.attach(io: File.open(path_image), filename: 'peruana.jpg')


puts 'Cadastrando Restaurantes'


# Italian Restaurants
path_image = 'public/images/restaurants/luigi.png'
r = Restaurant.create!(
	id: 1,
	name: 'Luigi Pizzas',
	description: 'Sua pizza, do seu jeito.',
	status: 'open', delivery_tax: 5.50,
	state: 'RJ', city: 'Barra Mansa', street: 'Rua Eduardo Junqueira',
	number: '388', neighborhood: 'Centro', category_id: 2
	)
r.image.attach(io: File.open(path_image), filename: 'luigi.png')
pz = ProductCategory.create!(title: 'Pizzas', restaurant: r)
lt = ProductCategory.create!(title: 'Bebidas', restaurant: r)

#prod = Product.create!(name: 'MUSSARELA', description: 'Mussarela.', price:22, product_category: pz) 
#prod.image.attach(io: File.open('public/images/products/MUSSARELA.png'), filename: 'MUSSARELA.png')


prod = Product.create!(name: 'CALABRESA', description: 'Mussarela, calabresa e cebola.', price:28, product_category: pz) 
prod.image.attach(io: File.open('public/images/products/CALABRESA.png'), filename: 'CALABRESA.png')


prod = Product.create!(name: 'BMC', description: 'Mussarela, bacon, milho e cheddar', price:31, product_category: pz) 
prod.image.attach(io: File.open('public/images/products/BMC.png'), filename: 'BMC.png')


#prod = Product.create!(name: 'LOMBO COM CHEDDAR', description: 'Mussarela, lombo e cheddar.', price:31, product_category: pz) 
#prod.image.attach(io: File.open('public/images/products/LOMBOCOMCHEDDAR.png'), filename: 'LOMBOCOMCHEDDAR.png')


prod = Product.create!(name: 'VEGETARIANA', description: 'Mussarela, tomate, pimentão, ovo, palmito e catupiry.', price:30, product_category: pz) 
prod.image.attach(io: File.open('public/images/products/VEGETARIANA.png'), filename: 'VEGETARIANA.png')

prod = Product.create!(name: 'FRANGO COM CATUPIRY', description: 'Mussarela, frango e catupiry. ', price:30, product_category: pz) 
prod.image.attach(io: File.open('public/images/products/FRANGOCOMCATUPIRY.png'), filename: 'FRANGOCOMCATUPIRY.png')


#prod = Product.create!(name: 'FRANGO COM CHEDDAR', description: 'Mussarela, frango E cheddar', price:30, product_category: pz) 
#prod.image.attach(io: File.open('public/images/products/FRANGOCOMCHEDDAR.png'), filename: 'FRANGOCOMCHEDDAR.png')


#prod = Product.create!(name: 'ABC', description: 'Mussarela, alho, bacon e champignon', price:32, product_category: pz) 
#prod.image.attach(io: File.open('public/images/products/ABC.png'), filename: 'ABC.png')


prod = Product.create!(name: 'PORTUGUESA', description: 'Mussarela, presunto, ovo, tomate, cebola, milho e azeitona.', price:33, product_category: pz) 
prod.image.attach(io: File.open('public/images/products/PORTUGUESA.png'), filename: 'PORTUGUESA.png')


prod = Product.create!(name: 'QUATRO QUEIJOS', description: 'Mussarela, parmesão, gorgonzola e catupiry.', price:33, product_category: pz) 
prod.image.attach(io: File.open('public/images/products/QUATROQUEIJOS.png'), filename: 'QUATROQUEIJOS.png')


#prod = Product.create!(name: 'CINCO QUEIJOS', description: 'Mussarela, parmesão, gorgonzola, catupiry e cheddar', price:36, product_category: pz) 
#prod.image.attach(io: File.open('public/images/products/CINCOQUEIJOS.png'), filename: 'CINCOQUEIJOS.png')

prod = Product.create!(name: 'COCA-COLA 2L', price: 9, description: '',  product_category: lt)
prod.image.attach(io: File.open('public/images/products/COCA2L.png'), filename: 'COCA2L.png')

prod = Product.create!(name: 'MANTIQUEIRA 2L', price: 6, description: '',  product_category: lt)
prod.image.attach(io: File.open('public/images/products/MANTIQUEIRA2L.png'), filename: 'MANTIQUEIRA2L.png')