class AdminController < ApplicationController
	def index
		@openorders = Order.all.order(:id)
		render json: @openorders
	end



	def show
		render json: @openorders, order_products: true
	end

	def search
		@restaurants = Restaurant.search(name_or_description_cont: params[:q]).result
		@restaurants = @restaurants.near(params[:city]) if params[:city]
		
		render json: @restaurants
	end

	private

	def filter_by_category
		@restaurants = @restaurants.select do |r|
			r.category.title == params[:category]
		end
	end

	def set_restaurant
		@restaurant = Restaurant.find(params[:id])
	end
end
